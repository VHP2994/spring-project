package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Book;
import com.example.demo.service.BookService;

@RestController
public class BoookController {

	@Autowired
	BookService bookService;
	
	@GetMapping("/book/getbooks")
	public List<Book> getAllBooks(){
		return bookService.listBook();
	}
	
	@GetMapping("/book/getById/{id}")
	public ResponseEntity<Book> getBookById(@PathVariable("id") Integer id) {
		Book book=bookService.findBookById(id);
		
		if(book==null) {
			return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
		}
		else {
			return new ResponseEntity<Book>(HttpStatus.OK);
		}
	}
	
	@PostMapping("/book/createbook")
	public ResponseEntity<Void> createNewBook(@RequestBody Book book){
		bookService.createBook(book);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@PutMapping("/book/updatebook/{id}")
	public ResponseEntity<Book> updateBook(@RequestBody Book currentBook,@PathVariable("id") Integer id){
		Book book=bookService.findBookById(id);
		currentBook.setId(id);
		bookService.update(currentBook, id);
		return new ResponseEntity<Book>(HttpStatus.OK);
	}
	
	@DeleteMapping("/book/deletebook/{id}")
	public ResponseEntity<Void> deleteBook(@PathVariable("id") Integer id){
		bookService.deleteBook(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
