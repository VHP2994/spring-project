package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Book;
import com.example.demo.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	BookRepository bookRepository;
	
	public List<Book> listBook() {
		return bookRepository.findAll();
	}
	
	public Book findBookById(Integer id) {
		Optional<Book> book=bookRepository.findById(id);
		Book notNullBook=book.get();
		return notNullBook;
	}
	
	public void createBook(Book book) {
		bookRepository.save(book);
	}
	
	public Book update(Book book,Integer id) {
		return bookRepository.save(book);
	}
	
	public void deleteBook(Integer id) {
		bookRepository.deleteById(id);
	}
}
